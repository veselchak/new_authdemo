# FastAPI server

import base64
import hmac
import json
import hashlib
from typing import Optional

from fastapi import FastAPI, Form, Cookie
from fastapi import responses
from fastapi.responses import Response
from starlette.requests import cookie_parser

app = FastAPI()

SECRET_KEY = 'f536c23f15e496431a7bbcf888454496719c37ddbab84389a5dd36d7e3c7fea0'
PASSWORD_SALT = 'c1d47de811e19165350fb941ea49802b7acf84ad9cf8ed9ff106771e47677ba8'

def sign_data(data: str) -> str:
    '''Возвращает подписанные данные data'''
    return hmac.new(
        SECRET_KEY.encode(),
        msg=data.encode(),
        digestmod=hashlib.sha256
    ).hexdigest().upper()

def get_username_from_signed_string(username_signed: str) -> Optional[str]:
    try:
        username_base64, sign = username_signed.split('.')
    except ValueError:
        return None
    
    username = base64.b64decode(username_base64.encode()).decode()
    valid_sign = sign_data(username)
    if hmac.compare_digest(valid_sign, sign):
        return username

def verify_password(username: str, password: str) -> bool:
    password_hash = hashlib.sha256((password + PASSWORD_SALT).encode()).hexdigest().lower()
    stored_password_hash = users[username]['password'].lower()
    return  stored_password_hash == password_hash

users = {
    'a@ru': {
        'name': 'Alex',
        'password': 'a50e7e31a8995adc8714c64e4c74a81358290400d2fe2dbb28c6f8a5b112fa18',
        'balance': 100_000
    },
    'b@ru': {
        'name': 'Bob',
        'password': '899c66993b8e54921db642bbaf3109aa6ced00e487fb737b83e1bddcffddd883',
        'balance': 200_000
    }
}

@app.get('/')
def index_page(username: Optional[str] = Cookie(default=None)):
    with open('templates/login.html', 'r') as f:
        login_page = f.read()
    if not username:
        response = Response(login_page, media_type='text/html')
        response.delete_cookie(key='username')
        return response

    valid_username = get_username_from_signed_string(username)
    if not valid_username:
        response = Response(login_page, media_type='text/html')
        response.delete_cookie(key='username')
        return response
    try:
        user = users[valid_username]
    except KeyError:
        response = Response(login_page, media_type='text/html')
        response.delete_cookie(key='username')
        return response
    return Response(f'Привет, {users[valid_username]["name"]}!', media_type='text/html')

@app.post('/login')
def process_login_page(username: str = Form(...), password: str = Form(...)):
    user = users.get(username)
    if not user or not verify_password(username, password):
        return Response(
            json.dumps({
                "success": False,
                "message": "Я вас не знаю!"
            }),
            media_type="application/json")
    
    response = Response(
        json.dumps({
                "success": True,
                "message": f"Привет, {user['name']}!</br>Баланс: {user['balance']} eur"
            }),
            media_type="application/json")
    
    username_signed = base64.b64encode(username.encode()).decode() + '.' + \
        sign_data(username)
    response.set_cookie(key='username', value=username_signed)
    return response
